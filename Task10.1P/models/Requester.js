const mongoose = require("mongoose")

const userSchema = new mongoose.Schema({
    taskType: String,
    title: String,
    description: String,
    settingUpTask: String,
    requireMasterWorkers: String,
    rewardPerResponse: String,
    numberOfWorkers: String
})

module.exports = mongoose.model("Requester", userSchema)