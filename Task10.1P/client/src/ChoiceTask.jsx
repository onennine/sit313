import React, { Component } from 'react'

export default class ChoiceTask extends Component {
  state={
    isTask1:false,
    isTask2:false,
    isTask3:false
  }
  handleChange= (e) =>{
    this.setState({[e.target.name]: e.target.checked})
    if(e.target.name==='isTask1')
    {
      this.state.isTask1=e.target.checked
    }else if (e.target.name==='isTask2')
    {
      this.state.isTask2=e.target.checked
    }
    else {this.state.isTask3=e.target.checked}
    this.props.checkBox("settingUpTask",this.state)
  }
  
  render() {
    const {isTask1,isTask2,isTask3}=this.state;
    return ( 
      <form>
      <label>
        <input
          type='checkbox' 
          checked={isTask1}
          name='isTask1'
          onChange={this.handleChange}
        />
        This is task one!
      </label>
      <br></br>
      <label>
        <input 
          type='checkbox' 
          checked={isTask2}
          name='isTask2'
          onChange={this.handleChange}
        />
        This is task two!
      </label>
      <br></br>
      <label>
        <input
          type='checkbox' 
          label='Task3'
          checked={isTask3}
          name='isTask3'
          onChange={this.handleChange}
        />
        This is task three!
      </label>
      </form>
    )
  }
}
