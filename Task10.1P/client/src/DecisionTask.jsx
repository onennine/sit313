import React, { Component } from 'react'
import { Form, Radio } from 'semantic-ui-react'

export default class DecisionTask extends Component {
  state = {}
  handleChange = (e, { value }) => {
    this.setState({ value })
    this.props.radioButton("settingUpTask",value)
  }
  render() {
    return (
      <Form>
        <Form.Field>
          Decision-Making Task: <b>{this.state.value}</b>
        </Form.Field>
        <Form.Field>
          <Radio
            label='True'
            name='radioGroup'
            value='true'
            checked={this.state.value === 'true'}
            onChange={this.handleChange}
          />
        </Form.Field>
        <Form.Field>
          <Radio
            label='False'
            name='radioGroup'
            value='false'
            checked={this.state.value === 'false'}
            onChange={this.handleChange}
          />
        </Form.Field>
      </Form>
    )
  }
}