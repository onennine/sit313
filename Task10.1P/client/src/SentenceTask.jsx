import React from 'react'
import { Form } from 'semantic-ui-react'

const SentenseTask = (props) => (
  <Form>
    <p>Sentence-Level Task:</p>
    <Form.Field 
    name={props.name}
    value={props.value}
    onChange = {props.onChange}
    label='What kind of task do you like?' 
    control='textarea' 
    rows='3' />
  </Form>
)

export default SentenseTask