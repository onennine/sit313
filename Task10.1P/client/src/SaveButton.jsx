import React from 'react'

const saveButton=(props)=>{
    return (
    <button type={props.type} onClick={props.onClick}> {props.text}</button>
    )
}

export default saveButton