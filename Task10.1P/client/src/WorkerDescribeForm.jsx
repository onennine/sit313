import React from 'react'
import { Form} from 'semantic-ui-react'

const WorkerDescribeForm = (props) => (
  <Form>
    <Form.Field >
      <Form.Input 
        name={props.name}
        value={props.value}
        label='Title' 
        onChange = {props.onChange}
        width={8} />
    </Form.Field>
    <Form.Field >
      <Form.Input 
        name={props.name1}
        value={props.value1}
        onChange = {props.onChange1}
        label='Description' 
        width={8} />
    </Form.Field>
    <Form.Field >
      <Form.Input label='Expiry Date' width={8} />
    </Form.Field>
  </Form>
)

export default WorkerDescribeForm