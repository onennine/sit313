import React, { Component } from 'react'
import { Form } from 'semantic-ui-react'

class WorkerRequirementForm extends Component {
  state = {}

  handleChange = (e, { value }) => {
    this.setState({ value })
    this.props.radioButton("requireMasterWorkers",value)
  }

  render() {
    const { value } = this.state
    return (
      <Form>
        <Form.Group inline>
          <label>Require Master Workers</label>
          <Form.Radio
            label='Yes'
            value='yes'
            checked={value === 'yes'}
            onChange={this.handleChange}
          />
          <Form.Radio
            label='No'
            value='no'
            checked={value === 'no'}
            onChange={this.handleChange}
          />
        </Form.Group>
        <Form.Field >
            <Form.Input 
            label='Reward per response' 
            name={this.props.name}
            value={this.props.value}
            onChange = {this.props.onChange}
            width={8} />
        </Form.Field>
        <Form.Field >
            <Form.Input 
            label='Number of workers' 
            name={this.props.name1}
            value={this.props.value1}
            onChange = {this.props.onChange1}
            width={8} />
        </Form.Field>
      </Form>
    )
  }
}

export default WorkerRequirementForm