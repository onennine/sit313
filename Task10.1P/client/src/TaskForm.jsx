import React , { useState }from 'react'

const TaskForm=(props)=>{
    const [radio, setRadio]=useState("choice")
    const handleChange=(e)=>{
        setRadio(e.target.value)
        props.chooseTaskType("taskType",e.target.value)
    }
    return (
        <form>
        <input type="radio"
        checked={radio === 'choice'}
        value='choice'                
        onChange={handleChange}
        />
        <label>Choice Task</label>
        <br/>
        <input type="radio"
        checked={radio === 'decision'}
        value='decision'                
        onChange={handleChange}
        />
        <label>Decision-Making Task</label>
        <br/>
        <input type="radio"
        checked={radio === 'sentence'}
        value='sentence'                
        onChange={handleChange}
        />
        <label>Sentence-Level Task</label>
        <br/>  
        </form>
    )
}

export default TaskForm
