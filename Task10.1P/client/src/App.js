import React , { useState }from 'react'
import './App.css'
import TaskForm from './TaskForm'
import WorkerDescribeForm from './WorkerDescribeForm'
import WorkerRequirementForm from './WorkerRequirementForm'
import SaveButton from './SaveButton'
import ChoiceTask from './ChoiceTask'
import DecisionTask from './DecisionTask'
import SentenceTask from './SentenceTask'

function App(props){
    const [content, setContent]=useState({
        taskType: '', 
        title: '' ,
        description: '', 
        settingUpTask: '',
        requireMasterWorkers: '',
        rewardPerResponse: '',
        numberOfWorkers: ''
    })
    const [task, setTask]=useState("choice")
    const chooseTaskType=(name,value)=>{
        setTask(value)
        setContent ((preValue)=>{ 
        return {
        ...preValue,
        [name]: value,
        }
        })
    }

    const radioButton=(name,value)=>{
        setContent ((preValue)=>{ 
        return {
        ...preValue,
        [name]: value,
        }
        })
    }

    const checkBox=(name,list)=>{
        var mylist=''
        if(list.isTask1===true){mylist=mylist+" Task1"}else{}
        if(list.isTask2===true){mylist=mylist+" Task2"}else{}
        if(list.isTask3===true){mylist=mylist+" Task3"}else{}
        setContent ((preValue)=>{ 
        return {
        ...preValue,
        [name]: mylist,
        }
        })
    }

    const handleChange = (event)=>{
        const {name, value} = event.target
        const {name1, value1} = event.target
        setContent ((preValue)=>{ 
        return {
        ...preValue,
        [name]: value,
        [name1]: value1
        }
        })
    }

    const handleClick = ()=>{
        fetch('http://localhost:5000/task', {
            method: 'post',
            headers: {'Content-Type': 'application/json'},
            body : JSON.stringify({
                taskType: content.taskType,
                title: content.title,
                description: content.description,
                settingUpTask: content.settingUpTask,
                requireMasterWorkers: content.requireMasterWorkers,
                rewardPerResponse: content.rewardPerResponse,
                numberOfWorkers: content.numberOfWorkers
            })
        })
        .then(response => response.json())
        .then(data => console.log(data))
        .catch(err => {
            console.log("Error:" + err)
        })
    }

    return(
        <div className="wapper">
            <h4>New Requester Task</h4>
            <p>Select Task Type:</p>
            <TaskForm chooseTaskType={chooseTaskType} />
            <h4>Describe your task to Workers</h4>
            <WorkerDescribeForm 
                name= "title"
                onChange = {handleChange}
                value = {content.title}

                name1= "description"
                onChange1 = {handleChange}
                value1 = {content.description}
            />
            <h4>Setting up your Task</h4>
            
            {
                task==='choice'?
                <ChoiceTask checkBox={checkBox}/>
                : task==='decision'?
                <DecisionTask radioButton={radioButton} />
                : task==='sentence'?
                <SentenceTask 
                    name= "settingUpTask"
                    onChange = {handleChange}
                    value = {content.settingUpTask}
                />
                :
                <ChoiceTask />
            }
        
            <h4>Workers Requirement</h4>
            <WorkerRequirementForm 
                radioButton={radioButton}

                name= "rewardPerResponse"
                onChange = {handleChange}
                value = {content.rewardPerResponse}

                name1= "numberOfWorkers"
                onChange1 = {handleChange}
                value1 = {content.numberOfWorkers}
            />
            <br></br>
            <SaveButton 
                type = 'submit'
                text='Save'
                onClick={handleClick}
                />
            <br></br>
            <br></br>
        </div>
    )
}

export default App;