import React, { Component } from 'react';
import { Form } from 'semantic-ui-react'

class TaskTypeForm extends Component {
    state = {}
  
    handleChange = (e, { value }) => this.setState({ value })
    render() {
      const { value } = this.state
      return (<div>
        <Form>
          <Form.Group inline>
            <label>Select Task Type</label>
            <Form.Radio
              label='Choice Task'
              value='choice'
              checked={value === 'choice'}
              onChange={this.handleChange}
            />
            <Form.Radio
              label='Decision-Making Task'
              value='decision'
              checked={value === 'decision'}
              onChange={this.handleChange}
            />
            <Form.Radio
              label='Sentence-Level Task'
              value='sentence'
              checked={value === 'sentence'}
              onChange={this.handleChange}
            />
          </Form.Group>
        </Form>
        <p>thsjdjhakd: {value}</p>
        </div>
      )
    }
  }
  
  export default TaskTypeForm