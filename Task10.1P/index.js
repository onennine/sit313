const express = require("express")
const bodyParser = require("body-parser")
const https = require("https")
const User = require("./models/Requester");
const mongoose = require("mongoose")
const cors = require("cors")
const multipart = require('connect-multiparty');
const multipartMiddleware = multipart()

const app = express()
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cors())
app.use(bodyParser.json())

// mongoose
mongoose.connect("mongodb://localhost:27017/Task10DB", { useNewUrlParser: true, useUnifiedTopology: true })

//home route
app.get('/', (req, res) => {
    const user = {
        title: "sit313",
        description: "sit313"
    }
    res.send(user)
})

//route
app.post('/task', multipartMiddleware, (req, res) => {
    const user = new User({
        taskType: req.body.taskType,
        title: req.body.title,
        description: req.body.description,
        settingUpTask: req.body.settingUpTask,
        requireMasterWorkers: req.body.requireMasterWorkers,
        rewardPerResponse: req.body.rewardPerResponse,
        numberOfWorkers: req.body.numberOfWorkers
    });
    user.save()
        .catch((err) => console.log(err));
    res.json(('saved to db: ' + user));
})

let port = process.env.PORT;
if (port == null || port == "") {
    port = 5000;
}

app.listen(port, (req, res) => {
    console.log("Server is running successfullly!")
})