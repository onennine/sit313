import React from 'react'
import header from './pexels-dark-indigo-2892962.jpg'
import './Image.css'

function Image(){
    return <img className="header" src={header} alt="header" />
}

export default Image;