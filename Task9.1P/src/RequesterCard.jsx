import React from 'react'
import { Card, Image } from 'semantic-ui-react'
import './RequesterCard.css'

const RequesterCard = (props) => {
    return <div className="column">
        <Card>
            <Image src={props.avatar} wrapped ui={false} />
            <Card.Content>
                <Card.Header>{props.name}</Card.Header>
                <Card.Description>
                    {props.positionDetail}
                </Card.Description>
            </Card.Content>
        </Card>
    </div>
}

export default RequesterCard;