import React, { Component } from "react";
import { Input, Button, Menu, Icon } from "semantic-ui-react";

export default class MenuExampleIcons extends Component {
  state = { activeItem: "gamepad" };

  handleItemClick = (e, { name }) => this.setState({ activeItem: name });

  render() {
    const { activeItem } = this.state;

    return (
      <Menu className="footer">
        <Menu.Item header>NEWSLETTER SIGN</Menu.Item>
        <Menu.Item>
          <Input placeholder="Enter your email" />
        </Menu.Item>

        <Menu.Item>
          <Button className="Subscribe">Subscribe</Button>
        </Menu.Item>

        <Menu.Item position="right">
          <Menu.Item header>CONNECT US</Menu.Item>
          <Menu icon>
            <Menu.Item
              name="gamepad"
              active={activeItem === "gamepad"}
              onClick={this.handleItemClick}
            >
              <Icon name="facebook" />
            </Menu.Item>

            <Menu.Item
              name="video camera"
              active={activeItem === "video camera"}
              onClick={this.handleItemClick}
            >
              <Icon name="twitter" />
            </Menu.Item>

            <Menu.Item
              name="video play"
              active={activeItem === "video play"}
              onClick={this.handleItemClick}
            >
              <Icon name="instagram" />
            </Menu.Item>
          </Menu>
        </Menu.Item>
      </Menu>
    );
  }
}
