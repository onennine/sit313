import React from 'react'
import RequesterCard from './RequesterCard'
import './RequesterCard.css'
import requesterList from './RequesterList'

const RequesterCardList= ()=>{
    return <div className="row">
    {requesterList.map(  (staff ) => 
    <RequesterCard 
        key = {staff.key}
        avatar = {staff.avatar}
        name = {staff.name}
        positionDetail = {staff.positionDetail}
    />
)}
    </div>
}

export default RequesterCardList;