import faker from 'faker'

const requesterList = []
var requesterNo = 6;
for (var i = 0; i < requesterNo; i++) {
    requesterList.push({
        "key": i,
        "avatar": faker.image.avatar(),
        "name": faker.name.firstName(),
        "positionDetail": faker.name.jobDescriptor()
    })
}
export default requesterList