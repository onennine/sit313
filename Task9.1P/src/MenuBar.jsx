import React, { Component } from 'react';
import { Button,Menu } from 'semantic-ui-react'

export default class MenuExampleBasic extends Component {
  state = {}

  handleItemClick = (e, { name }) => this.setState({ activeItem: name })

  render() {
    const { activeItem } = this.state

    return (
      <Menu>
        <Menu.Item >ICrowdTask</Menu.Item>
        <Menu.Item
          name='Howitworks'
          active={activeItem === 'How it works'}
          onClick={this.handleItemClick}
        >
          How it works
        </Menu.Item>

        <Menu.Item
          name='Requesters'
          active={activeItem === 'Requesters'}
          onClick={this.handleItemClick}
        >
          Requesters
        </Menu.Item>

        <Menu.Item
          name='Workers'
          active={activeItem === 'Workers'}
          onClick={this.handleItemClick}
        >
          Workers
        </Menu.Item>

        <Menu.Item
          name='Pricing'
          active={activeItem === 'Pricing'}
          onClick={this.handleItemClick}
        >
          Pricing
        </Menu.Item>

        <Menu.Item
          name='About'
          active={activeItem === 'About'}
          onClick={this.handleItemClick}
        >
          About
        </Menu.Item>

        <Menu.Menu position='right'></Menu.Menu>
        <Menu.Item>
          <Button className="signin">Sign in</Button>
        </Menu.Item>
      </Menu>
    )
  }
}
        