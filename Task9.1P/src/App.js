import React from 'react'
import './App.css'
import MenuBar from './MenuBar'
import Image from './Image'
import RequesterCardList from './RequesterCardList'
import Footer from './Footer'

function App(){
    return(<div className="wapper">
        <div className="top">
            <MenuBar />
            <Image />
            <h1>Featured Requesters</h1>
            <RequesterCardList />
        </div>
        <div className="bottom"> 
            <Footer /> 
        </div >  
        </div>
    )
}

export default App;